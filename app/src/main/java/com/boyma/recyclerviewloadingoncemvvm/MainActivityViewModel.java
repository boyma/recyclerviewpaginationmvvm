package com.boyma.recyclerviewloadingoncemvvm;

import android.annotation.SuppressLint;
import android.arch.lifecycle.ViewModel;
import android.databinding.ObservableField;
import android.databinding.ObservableInt;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.boyma.recyclerviewloadingoncemvvm.app.App;
import com.boyma.recyclerviewloadingoncemvvm.net.netpostfeature.di.DaggerNetPostComponent;
import com.boyma.recyclerviewloadingoncemvvm.net.netpostfeature.di.NetPostComponent;
import com.boyma.recyclerviewloadingoncemvvm.net.netpostfeature.models.ItemType1;
import com.boyma.recyclerviewloadingoncemvvm.net.netpostfeature.models.ItemType2;
import com.boyma.recyclerviewloadingoncemvvm.net.netpostfeature.models.responses.PagesDto;
import com.boyma.recyclerviewloadingoncemvvm.net.netpostfeature.models.responses.PostDto;
import com.github.nitrico.lastadapter.LastAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

public class MainActivityViewModel extends ViewModel {

    //ui
    public ObservableField<LastAdapter> recadapter = new ObservableField<>();
    public ObservableInt showLoading = new ObservableInt(View.VISIBLE);
    //

    private CompositeDisposable subcriber = new CompositeDisposable();

    private NetPostComponent netPostComponent;
    private ArrayList<Object> posts = new ArrayList<>();

    private int maxPages;
    private int currentPage = 1;
    private boolean isLoading;

    public MainActivityViewModel() {
        initComponent();
        initAdapter();
        unionLoadPosts();
    }

    private void initAdapter() {
        recadapter.set(new LastAdapter(posts, BR.item)
                .map(ItemType1.class, R.layout.item_type1)
                .map(ItemType2.class, R.layout.item_type2)
                .map(LoadingFooter.class, R.layout.footer));

    }

    private void setNormalMode() {
        showLoading.set(View.GONE);
    }

    private void setLoadingMode() {
        showLoading.set(View.VISIBLE);
    }

    //-----///////////////////////////////PAGINATION RECYCLER/////////////////////////////////////
    @SuppressLint("CheckResult")
    private void unionLoadPosts() {
        setLoadingMode();
        isLoading = true;
        subcriber.add(netPostComponent.getIPostDataRepository().getPagesCount()
                .delay(5000, TimeUnit.MILLISECONDS)
                .flatMap((Function<PagesDto, Single<List<PostDto>>>) pagesDto -> {
                    maxPages = pagesDto.getPages();
                    System.out.println("loading:" + currentPage);
                    return netPostComponent.getIPostDataRepository().getPosts(currentPage);
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<PostDto>>() {
                    @Override
                    public void onSuccess(List<PostDto> postDtos) {
                        isLoading = false;
                        setNormalMode();

                        removeFooter();
                        posts.addAll(extrudeList(postDtos));
                        if (!isLastPage()) posts.add(new LoadingFooter());
                        recadapter.get().notifyDataSetChanged();

                    }

                    @Override
                    public void onError(Throwable e) {
                        isLoading = false;
                        setNormalMode();
                        e.printStackTrace();
                    }
                })
        );
    }

    private void removeFooter() {
        System.out.println("removeFooter");
        if (!posts.isEmpty() && !isLoading) {
            posts.remove(posts.size() - 1);
            System.out.println("removeFooterRemove");
        }
    }

    public RecyclerView.OnScrollListener getRecyclerViewOnScrollListener() {
        return new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = recyclerView.getLayoutManager().getChildCount();
                int totalItemCount = recyclerView.getLayoutManager().getItemCount();
                int firstVisibleItemPosition = ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();

                if (!isLoading && !isLastPage()) {
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= maxPages) {

                        System.out.println("onScrolledloadPostsFromPage");
                        currentPage++;
                        unionLoadPosts();
                    }
                }
            }
        };
    }

    private boolean isLastPage() {
        return currentPage == maxPages;
    }


    private ArrayList<Object> extrudeList(List<PostDto> postDtos) {
        ArrayList<Object> al = new ArrayList<>();
        for (PostDto dto : postDtos) {
            if (dto.getId() == 1) {
                al.add(new ItemType1(dto));
            } else {
                al.add(new ItemType2(dto));
            }
        }
        return al;
    }

    //-----///////////////////////////////PAGINATION RECYCLER/////////////////////////////////////

    private void initComponent() {
        netPostComponent = DaggerNetPostComponent.builder()
                .netComponent((App.getNetComponent()))
                .build();
    }

    public void deleteItem(int i) {
        posts.remove(i);
        posts.remove(i);
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        subcriber.clear();
    }
}
