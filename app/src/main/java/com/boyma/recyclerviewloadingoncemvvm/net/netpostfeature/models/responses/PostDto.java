package com.boyma.recyclerviewloadingoncemvvm.net.netpostfeature.models.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PostDto {


    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("page")
    @Expose
    private String page;

    public String getTitle() {
        return title;
    }

    public int getId() {
        return id;
    }
}
