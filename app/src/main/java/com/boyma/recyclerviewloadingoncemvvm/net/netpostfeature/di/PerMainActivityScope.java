package com.boyma.recyclerviewloadingoncemvvm.net.netpostfeature.di;

import javax.inject.Scope;

@Scope
public @interface PerMainActivityScope {
}
