package com.boyma.recyclerviewloadingoncemvvm.net.netpostfeature.models;

import com.boyma.recyclerviewloadingoncemvvm.net.netpostfeature.models.responses.PostDto;

public class ItemType2 {

    public PostDto getDto() {
        return dto;
    }

    private PostDto dto;

    public ItemType2(PostDto dto) {
        this.dto = dto;
    }
}
