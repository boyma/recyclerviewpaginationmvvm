package com.boyma.recyclerviewloadingoncemvvm.net.netpostfeature.models.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PagesDto {
    @SerializedName("pages")
    @Expose
    private Integer pages;

    public Integer getPages() {
        return pages;
    }
}
