package com.boyma.recyclerviewloadingoncemvvm.net.netpostfeature;

import com.boyma.recyclerviewloadingoncemvvm.net.netpostfeature.models.responses.PagesDto;
import com.boyma.recyclerviewloadingoncemvvm.net.netpostfeature.models.responses.PostDto;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Api {

    @GET("B0yma/dbtest/pages")
    Single<PagesDto> getPagesCount();

    @GET("B0yma/dbtest/posts")
    Single<List<PostDto>> getPosts(@Query("page") int page);
}
