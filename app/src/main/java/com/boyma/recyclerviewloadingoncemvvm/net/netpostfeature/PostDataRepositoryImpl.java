package com.boyma.recyclerviewloadingoncemvvm.net.netpostfeature;


import com.boyma.recyclerviewloadingoncemvvm.net.netpostfeature.models.responses.PagesDto;
import com.boyma.recyclerviewloadingoncemvvm.net.netpostfeature.models.responses.PostDto;

import java.util.List;

import io.reactivex.Single;

public class PostDataRepositoryImpl implements IPostDataRepository{

    private Api api;


    public PostDataRepositoryImpl(Api api) {
        this.api = api;
        System.out.println("PostDataRepositoryImpl constructor");
    }

    @Override
    public Single<List<PostDto>> getPosts(int page) {
        return api.getPosts(page);
    }

    @Override
    public Single<PagesDto> getPagesCount() {
        return api.getPagesCount();
    }
}
