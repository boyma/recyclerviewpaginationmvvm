package com.boyma.recyclerviewloadingoncemvvm.app.di;

import javax.inject.Scope;

@Scope
public @interface AppScope {
}
