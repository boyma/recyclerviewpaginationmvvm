package com.boyma.recyclerviewloadingoncemvvm;

import android.databinding.BindingAdapter;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

public class BindingAdapters {

    @BindingAdapter({"app:setLinAdapter"})
    public static void setLinAdapter(RecyclerView view, RecyclerView.Adapter adapter) {
        view.setLayoutManager(new LinearLayoutManager(view.getContext()));
        view.setAdapter(adapter);
    }

    @BindingAdapter({"app:setOnScrollListener"})
    public static void setLinAdapter(RecyclerView view, RecyclerView.OnScrollListener listener) {
        view.addOnScrollListener(listener);
    }

}
